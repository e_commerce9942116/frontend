import React from 'react'
import './Breacrums.css'
import arrow_icon from '../Assets/breadcrum_arrow.png'

export default function Breacrums(props) {
  const {product} = props;
  return (
    <div className='breacrums'>
      HOME <img src={arrow_icon} alt="" /> 
      SHOP <img src={arrow_icon} alt="" /> {product.category} 
      <img src={arrow_icon} alt="" /> {product.name}
    </div>
  )
}
